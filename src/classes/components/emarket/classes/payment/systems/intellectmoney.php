<?php

class intellectmoneyPayment extends payment {

    public function validate() {
        return true;
    }

    public function process($template = null) {
        $this->order->order();

        $eshopId = $this->object->eshop_id;
        $orderId = $this->order->getId();
        $serviceName = "Оплата заказа №" . $orderId;
        $secretKey = $this->object->sekret_key;
        $customer = customer::get();
        $email = $customer->getValue('e-mail');
        if (!$email) {
            $email = $customer->getValue('email');
        }

        $currency = $this->object->is_test ? 'TST' : strtoupper(mainConfiguration::getInstance()->get('system', 'default-currency'));

        $amount = number_format($this->order->getActualPrice(), 2, '.', '');
        $holdMode = !empty($this->object->is_hold) ? 1 : 0;
        if ($holdMode) {
            $expireDate = $this->object->expire_date;
            if (is_numeric($expireDate) && $expireDate > 0 && $expireDate < 120) {
                $expireDate = date('Y-m-d H:i:00', strtotime('+' . $expireDate . ' hours'));
            } else {
                $expireDate = date('Y-m-d H:i:00', strtotime('+ 72 hours'));
            }
        }

        $hash = md5(join('::', array($eshopId, $orderId, $serviceName, $amount, $currency, $secretKey)));
        $param = array();
        $param["formAction"] = "https://merchant.intellectmoney.ru/ru/";
        $param["eshopId"] = $eshopId;
        $param["orderId"] = $orderId;
        $param["serviceName"] = $serviceName;
        $param["recipientAmount"] = $amount;
        $param["recipientCurrency"] = $currency;
        $param["user_email"] = $email;
        $param["holdMode"] = $holdMode;
        $param["expireDate"] = $expireDate;
        $param["hash"] = $hash;

        $this->order->setPaymentStatus('initialized');
        list($templateString) = emarket::loadTemplates(
                        "emarket/payment/intellectmoney/" . $template, "form_block"
        );

        return emarket::parseTemplate($templateString, $param);
    }

    public function error(&$buffer, $message) {
        $buffer->push($message);
        $buffer->end();
    }

    public function poll() {
        $buffer = outputBuffer::current();
        $buffer->clear();
        $buffer->contentType('text/plain');
        $paidInvoice = $this->object->paid_invoice_status;
        $createdInvoice = $this->object->create_invoice_status;
        $holdInvoice = $this->object->hold_invoice_status;
        $canceledInvoice = $this->object->canceled_invoice_status;
        $underpaidInvoice = $this->object->underpaid_invoice_status;
        $secretKey = $this->object->sekret_key;
        $requestSecretKey = getRequest('secretKey');
        $requestEshopId = getRequest('eshopId');
        $eshopId = $this->object->eshop_id;

        if (!in_array($_SERVER['REMOTE_ADDR'], array("194.147.107.254", "91.212.151.242", "127.0.0.1"))) {
            $this->error($buffer, 'ERROR: IP MISMATCH!');
        }
        if (!empty($requestSecretKey) && $requestSecretKey == $secretKey) {

            if ($requestSecretKey != $secretKey) {
                $this->error($buffer, 'ERROR: SECRET_KEY MISMATCH!');
            }
        }

        if ($requestEshopId != $eshopId) {
            $this->error($buffer, 'ERROR: INCORRECT ESHOP_ID!');
        }
        if (!$this->checkSignature()) {
            $this->error($buffer, 'ERROR: HASH MISMATCH!');
        }

        if ($checkAmount = number_format($this->order->getActualPrice(), 2, '.', '') != getRequest('recipientAmount')) {
            $this->error($buffer, 'ERROR: AMOUNT/CURRENCY MISMATCH!');
        }
        $orderPaymentStatus = $this->order->getPaymentStatus();
        $orderStatus = $this->order->getOrderStatus();
        $status_text = array($createdInvoice => 'CREATED', $canceledInvoice => 'CANCELED', $paidInvoice => 'PAID', $holdInvoice => 'HOLD', $underpaidInvoice => 'UNDER PAID');
        if (getRequest('paymentStatus') != 4) {
            if (getRequest('paymentStatus') == 3) {
                if (in_array($orderPaymentStatus, array($canceledInvoice, $paidInvoice, $holdInvoice, $underpaidInvoice))) {
                    $this->error($buffer, "ERROR: YOU CAN NOT CHANGE THE STATUS. FROM $status_text[$orderPaymentStatus] TO $status_text[$createdInvoice].");
                }
                $this->order->setPaymentStatus($createdInvoice);
            } elseif (getRequest('paymentStatus') == 5) {
                if ($orderPaymentStatus == $canceledInvoice) {
                    $this->error($buffer, "ERROR: YOU CAN NOT CHANGE THE STATUS. FROM $status_text[$orderPaymentStatus] TO $status_text[$paidInvoice].");
                }
                $this->order->setPaymentStatus($paidInvoice);
            } elseif (getRequest('paymentStatus') == 6) {
                if (in_array($orderPaymentStatus, array($canceledInvoice, $paidInvoice, $underpaidInvoice))) {
                    $this->error($buffer, "ERROR: YOU CAN NOT CHANGE THE STATUS. FROM $status_text[$orderPaymentStatus] TO $status_text[$holdInvoice].");
                }
                $this->order->setPaymentStatus($holdInvoice);
            } elseif (getRequest('paymentStatus') == 7) {
                if (in_array($orderPaymentStatus, array($canceledInvoice, $paidInvoice, $holdInvoice))) {
                    $this->error($buffer, "ERROR: YOU CAN NOT CHANGE THE STATUS. FROM $status_text[$orderPaymentStatus] TO $status_text[$underpaidInvoice].");
                }
                $this->order->setPaymentStatus($underpaidInvoice);
            }
        } elseif($orderPaymentStatus != $canceledInvoice) {
            $this->order->setPaymentStatus($canceledInvoice);
        }

        $buffer->push("OK");
        $buffer->end();
    }

    private function checkSignature() {
        $eshopId = getRequest('eshopId');
        $orderId = getRequest('orderId');
        $serviceName = getRequest('serviceName');
        $eshopAccount = getRequest('eshopAccount');
        $recipientAmount = getRequest('recipientAmount');
        $recipientCurrency = getRequest('recipientCurrency');
        $paymentStatus = getRequest('paymentStatus');
        $userName = getRequest('userName');
        $userEmail = getRequest('userEmail');
        $paymentData = getRequest('paymentData');
        $secretKey = $this->object->sekret_key;
        $hash = getRequest("hash");
        $control_hash_str = implode('::', array($eshopId, $orderId, $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus, $userName, $userEmail, $paymentData, $secretKey,));
        $control_hash = md5($control_hash_str);
        $control_hash_utf8 = md5(iconv('windows-1251', 'utf-8', $control_hash_str));
        $conv = iconv('windows-1251', 'utf-8', $control_hash_str);

        return !(($hash != $control_hash && $hash != $control_hash_utf8) || !$hash);
    }

}

?>