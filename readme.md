#Модуль оплаты платежной системы IntellectMoney для CMS UMI

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/UMI.CMS#whatnew.

Страница модуля на marketplace: https://market.umi-cms.ru/module/intellectmoney7/
<br>
Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/UMI.CMS#557741fb818c71c06c474a9479ba93d4c84b55
